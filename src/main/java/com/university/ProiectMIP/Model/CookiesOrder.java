package com.university.ProiectMIP.Model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "CookiesOrder")
public class CookiesOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String city;

    @OneToMany(mappedBy = "order")
    private List<Cookie> cookiesOrdered;

    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    public CookiesOrder() {

    }

    public CookiesOrder(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
