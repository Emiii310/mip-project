package com.university.ProiectMIP.Repos;

import com.university.ProiectMIP.Model.CookiesOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookiesOrderRepository extends JpaRepository<CookiesOrder, Long> {
}