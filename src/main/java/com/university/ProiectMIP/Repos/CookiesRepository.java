package com.university.ProiectMIP.Repos;

import com.university.ProiectMIP.Model.Cookie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookiesRepository extends JpaRepository<Cookie, Long> {
}