package com.university.ProiectMIP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProiectMipApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProiectMipApplication.class, args);
	}

}
