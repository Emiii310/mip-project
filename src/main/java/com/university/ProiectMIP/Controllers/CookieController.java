package com.university.ProiectMIP.Controllers;

import java.util.concurrent.atomic.AtomicLong;

import com.university.ProiectMIP.Model.Cookie;
import com.university.ProiectMIP.Repos.CookiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("cookie")
public class CookieController {

    private CookiesRepository repository;
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    CookieController(CookiesRepository repository) {
    this.repository = repository;
    }

    @GetMapping("/cookies")
    public Cookie getCookie(@PathVariable("id") Long id) {
        return repository.getOne(id);
    }

    @PostMapping("/cookie")
    public void addCookie(@RequestBody Cookie cookie) {
        repository.save(cookie);
    }
}