package com.university.ProiectMIP.Controllers;

import com.university.ProiectMIP.Model.CookiesOrder;
import com.university.ProiectMIP.Repos.CookiesOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("order")
public class CookiesOrderController {

    private final AtomicLong counter = new AtomicLong();
    private CookiesOrderRepository repository;

    @Autowired
    CookiesOrderController(CookiesOrderRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/cookies")
    public CookiesOrder getCookieOrder(@PathVariable("id") Long id) {
        return repository.getOne(id);
    }

    @PostMapping("/cookie")
    public void addCookie(@RequestBody CookiesOrder cookieOrder) {
        repository.save(cookieOrder);
    }

//    @PutMapping("/{id}")
//    CookiesOrder updateCookiesOrder(@PathVariable("id") Long id, @RequestBody @Valid CookiesOrder updatedCookiesOrder) {
//        return updateCookiesOrder(id, updatedCookiesOrder);
//    }
}