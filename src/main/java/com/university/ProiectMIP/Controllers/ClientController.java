package com.university.ProiectMIP.Controllers;

import com.university.ProiectMIP.Model.Client;
import com.university.ProiectMIP.Repos.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("client")
public class ClientController {

    private final AtomicLong counter = new AtomicLong();
    private ClientRepository repository;

    @Autowired
    ClientController(ClientRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/clients")
    public Client getClient(@PathVariable("id") Long id) {
        return repository.getOne(id);
    }

    @PostMapping("/client")
    public void addClient(@RequestBody Client client) {
        repository.save(client);
    }
}